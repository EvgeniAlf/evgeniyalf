#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"; fi

smart=$(grep smart= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smpau=$(grep smpau= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
dsksiz=$(grep dsksiz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
delolb=$(grep delolb= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cllgz=$(grep cllgz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clcache=$(grep clcache= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clkritzo=$(grep clkritzo= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
szchti=$(grep szchti= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
difilch=$(grep difilch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
vostrab=$(grep vostrab= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smonest=$(grep smonest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
mxsrmcp=$(grep mxsrmcp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
moncpu=$(grep moncpu= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
stpmod=$(grep stpmod= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
mstmd=$(grep mstmd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
stpsrv=$(grep stpsrv= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
minmem=$(grep minmem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
monmem=$(grep monmem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdcpser=$(grep cmdcpser= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdcpsrv=$(grep cmdcpsrv= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdcnsmxc=$(grep cmdcnsmxc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdmxcpuc=$(grep cmdmxcpuc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdservme=$(grep cmdservme= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdmemser=$(grep cmdmemser= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdconsmem=$(grep cmdconsmem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cmdmemcs=$(grep cmdmemcs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rebfix=$(grep rebfix= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smtscr=$(grep smtscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antifix=$(grep antifix= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chservs=$(grep chservs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

aumes=$(grep aumes= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ames=$(grep ames= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ticms=$(grep ticms= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ansto=$(grep ansto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
inscr=$(grep inscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antichat=$(grep antichat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
anthascr=$(grep anthascr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbascnz=$(grep tbascnz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ysnsc=$(grep ysnsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rest=$(grep rest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
zhki=$(grep zhki= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tsto=$(grep tsto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
timcomsc=$(grep timcomsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
asw=$(grep asw= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
autsav=$(grep autsav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
intcom=$(grep intcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
guardchat=$(grep guardchat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antihack=$(grep antihack= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tempba=$(grep tempba= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antihack=$(grep antihack= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
log=$(grep log= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

stwhi=0
chcpu=0
rmodul=1
prohd=0

# защита от двойного запуска
scrcheck=$(screen -ls | grep $smtscr | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/smart.log
screen -S $smtscr -X stuff "^C" >> $patch/logs/smart.log; fi

# защита от запуска вне скрина
screenfix=$(screen -ls | grep $smtscr | wc -m | sed s/[^0-9]//g)
if [ $screenfix -lt 5 ]; then
echo "запуск не в screen, исправление"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "запуск не в screen, исправление" $date >> $patch/logs/smart.log
screen -dmUS $smtscr bash -c "$patch/sh/smart.sh" && kill $$; fi

while [ $smart = "1" ]; do
# заданная пауза, кроме первого пуска
if [ $smpau -lt 5 ]; then smpau=5; fi
stwhi=$((stwhi+1))
if [ $stwhi != "1" ]; then sleep $((smpau*60)); fi

# проверка свободного места
if [ $szchti = "1" ]; then
cd /
if [ -f $patch/temp/du.txt ]; then rm $patch/temp/du.txt >> $patch/logs/smart.log; fi
du -s -B G --exclude=./proc >> $patch/temp/du.txt
line=$(wc -l $patch/temp/du.txt | sed s/[^0-9]//g)
size=$(head -n $line $patch/cfg/service-stop.cfg | tail -n 1 | sed s/[^0-9]//g)
rm $patch/temp/du.txt >> $patch/logs/smart.log
if [ $size -gt $dsksiz ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "===========> свободное место отсутствует:" $date >> $patch/logs/smart.log
echo "--->" $size >> $patch/logs/smart.log
if [ $delolb = "1" ]; then
old=$(ls -t $patch/backups/ | tail -n1)
echo "удаление старого бэкапа" $old >> $patch/logs/smart.log
rm -rf $patch/backups/$old/ >> $patch/logs/smart.log; fi
if [ $cllgz = "1" ]; then echo "чистка логов" >> $patch/logs/smart.log && rm -rf $patch/logs/* >> $patch/logs/smart.log; fi
if [ $clcache = "1" ]; then
echo "чистка кэша" >> $patch/logs/smart.log
rm -rf /var/cache/* >> $patch/logs/smart.log
mkdir /var/cache/debconf/ >> $patch/logs/smart.log; fi
if [ $clkritzo = "1" ]; then
echo "чистка не важных файлов" $patch/temp/ >> $patch/logs/smart.log
if [ -d $patch/temp/plugins/ ]; then rm -rf $patch/temp/plugins/* >> $patch/logs/smart.log; fi
if [ -d $patch/temp/server-files/ ]; then rm -rf $patch/temp/server-files/* >> $patch/logs/smart.log; fi
if [ -d $patch/temp/tempback/ ]; then rm -rf $patch/temp/tempback/* >> $patch/logs/smart.log; fi
if [ -d $patch/backups/.sync/ ]; then rm -rf $patch/backups/.sync/* >> $patch/logs/smart.log; fi
if [ -d $patch/crash-reports/ ]; then rm -rf $patch/crash-reports/* >> $patch/logs/smart.log; fi
if [ -d $patch/timings/ ]; then rm -rf $patch/timings/* >> $patch/logs/smart.log; fi; fi; fi; fi

# проверка файлов и папок
if [ $difilch = "1" ]; then
if ! [ -d $patch/temp/ ]; then mkdir $patch/temp/ >> $patch/logs/smart.log; fi
if ! [ -d $patch/temp/plugins/ ]; then mkdir $patch/temp/plugins/ >> $patch/logs/smart.log; fi
if ! [ -d $patch/backups/ ]; then mkdir $patch/backups/ >> $patch/logs/smart.log; fi
if ! [ -d $patch/backups-temp/ ]; then mkdir $patch/backups-temp/ >> $patch/logs/smart.log; fi
if ! [ -d $patch/logs/ ]; then mkdir $patch/logs/ >> $patch/logs/smart.log; fi
if ! [ -d $patch/logs/alt/ ]; then mkdir $patch/logs/alt/ >> $patch/logs/smart.log; fi

if ! [ -f $patch/cfg/antipund.cfg ]; then echo "дайте админку" > $patch/cfg/antipund.cfg; fi
if ! [ -f $patch/cfg/antispam.cfg ]; then echo "([0-9]{1,3}[\.]){3}[0-9]{1,3})" > $patch/cfg/antispam.cfg; fi
if ! [ -f $patch/cfg/antiswear.cfg ]; then echo "fuck" > $patch/cfg/antiswear.cfg; fi
if ! [ -f $patch/cfg/autogen.cfg ]; then echo "# you code" > $patch/cfg/autogen.cfg; fi
if ! [ -f $patch/cfg/automessage.cfg ]; then echo "you message" > $patch/cfg/automessage.cfg; fi
if ! [ -f $patch/cfg/back-dir.cfg ]; then echo "/var/log" > $patch/cfg/back-dir.cfg; fi
if ! [ -f $patch/cfg/back-file.cfg ]; then echo "/var/log/auth.log" > $patch/cfg/back-file.cfg; fi
if ! [ -f $patch/cfg/files-guard.cfg ]; then echo "server.properties" > $patch/cfg/files-guard.cfg; fi
if ! [ -f $patch/cfg/intcom.cfg ]; then echo "30=broadcast команда с интервалом 30 секунд" > $patch/cfg/intcom.cfg; fi
if ! [ -f $patch/cfg/pars.cfg ]; then echo "/ban" > $patch/cfg/pars.cfg; fi
if ! [ -f $patch/cfg/proxy.cfg ]; then echo "0.0.0.0" > $patch/cfg/proxy.cfg; fi
if ! [ -f $patch/cfg/remchat.cfg ]; then echo "<InterVi>" > $patch/cfg/remchat.cfg; fi
if ! [ -f $patch/cfg/remove.cfg ]; then echo "README.html" > $patch/cfg/remove.cfg; fi
if ! [ -f $patch/cfg/service-stop.cfg ]; then echo "bind" > $patch/cfg/service-stop.cfg; fi
if ! [ -f $patch/cfg/tempback.cfg ]; then echo "plugins/AuthMe/auths.db" > $patch/cfg/tempback.cfg; fi
if ! [ -f $patch/cfg/update.cfg ]; then echo "http://dev.bukkit.org/bukkit-plugins/worldedit/"; fi
if ! [ -f $patch/cfg/worlds.cfg ]; then echo "world" > $patch/cfg/worlds.cfg; fi

# действия в случае отсутствия главного конфига
if ! [ -f $patch/cfg/global.cfg ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "->" $date "отсутствуеет global.cfg" >> $patch/logs/smart.log
if [ -f $patch/cfg/global.cfg.old ]; then
mv $patch/cfg/global.cfg.old $patch/cfg/global.cfg >> $patch/logs/smart.log; else echo "errorch=1" > $patch/cfg/global.cfg; fi

checkserv=$(screen -ls | grep $srver | wc -m | sed s/[^0-9]//g)
if [ $vostrab = "1" ] && [ $checkserv -lt 5 ]; then
mem=$(cat /proc/meminfo | grep MemTotal | sed s/[^0-9]//g)
mem=$(echo $[mem/1024])
mem=$((mem-200))
mem=$(echo $mem"M")
cd $patch/
core=$(ls -f . | grep ".jar" | head -n1)

if [ -f $patch/server.log ]; then
datet=`/bin/date '+%H:%M:%S'`
dateY=`/bin/date '+%Y'`
dateM=`/bin/date '+%m'`
dateD=`/bin/date '+%d'`
if ! [ -d $patch/logs/alt/ ]; then mkdir $patch/logs/alt/; fi
if ! [ -d $patch/logs/alt/$dateY/ ]; then mkdir $patch/logs/alt/$dateY/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/; fi
if ! [ -d $patch/logs/alt/$dateY/$dateM/$dateD/ ]; then mkdir $patch/logs/alt/$dateY/$dateM/$dateD/; fi
zip -9 $patch/logs/alt/$dateY/$dateM/$dateD/$datet.zip $patch/server.log && rm $patch/server.log; fi

screen -dmUS $srver bash -c "java -Dfile.encoding=utf-8 -Xincgc -server -Xmx$mem -Xms256M -XX:MaxPermSize=128 -jar $patch/$core | tee server.log"
echo "---> восстановление работоспособности сервера" $date >> $patch/logs/smart.log; fi; fi; fi

# мониторинг загрузки процессора
if [ $moncpu = "1" ]; then
kcpu=$(nproc)
cpu=$(ps aux | awk '{s += $3} END {print s "%"}' | sed s/[^0-9]//g)
maxval=$((kcpu*100))
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "загрузка" $cpu"%"
if [ $cpu = $maxval ]; then chcpu=$((chcpu+1)); fi
# отправка команд
if [ $chcpu -gt $mxsrmcp ] && [ $cmdmxcpuc = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "загрузка" $cpu"%, выполнение команды" $cmdcnsmxc >> $patch/logs/smart.log
$cmdcnsmxc >> $patch/logs/smart.log; fi
if [ $chcpu -gt $mxsrmcp ] && [ $cmdcpsrv = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "загрузка" $cpu"%, отправка команды на сервер" $cmdcpser >> $patch/logs/smart.log
screen -S $srver -X eval "stuff '$cmdcpser'\015"; fi
# отключение модулей
if [ $chcpu -gt $mxsrmcp ] && [ $stpmod = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "загрузка" $cpu"%, отключение модулей" >> $patch/logs/smart.log
cp $patch/cfg/global.cfg $patch/cfg/global.cfg.old
timcheck=$(screen -ls | grep $ticms | wc -m | sed s/[^0-9]//g)
if [ $timcheck -gt 5 ]; then
sed 's/^timcom=.*/timcom=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
amecheck=$(screen -ls | grep $aumes | wc -m | sed s/[^0-9]//g)
if [ $amecheck -gt 5 ]; then
sed 's/^ames=.*/ames=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
intcheck=$(screen -ls | grep $inscr | wc -m | sed s/[^0-9]//g)
if [ $intcheck -gt 5 ]; then
sed 's/^intcom=.*/intcom=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
guacheck=$(screen -ls | grep $antichat | wc -m | sed s/[^0-9]//g)
if [ $guacheck -gt 5 ]; then
sed 's/^guardchat=.*/guardchat=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
anticheck=$(screen -ls | grep $anthascr | wc -m | sed s/[^0-9]//g)
if [ $anticheck -gt 5 ]; then
sed 's/^antihack=.*/antihack=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
tmpback=$(screen -ls | grep $tbascnz | wc -m | sed s/[^0-9]//g)
if [ $tmpback -gt 5 ]; then
sed 's/^tempba=.*/tempba=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi; fi; if [ $chcpu -gt $mxsrmcp ]; then chcpu=0; fi; fi

# мониторинг свободной оперативной памяти
if [ $monmem = "1" ]; then
fmem=$(free -m | grep Mem | awk '{print $4}')
if [ $fmem -lt $minmem ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "свободной памяти:" $fmem >> $patch/logs/smart.log
# отправка команд
if [ $cmdmemcs = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "свободной памяти:" $fmem", выполнение команды" $cmdconsmem >> $patch/logs/smart.log
$cmdconsmem >> $patch/logs/smart.log; fi
if [ $cmdmemser = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "свободной памяти:" $fmem", отправка команды на сервер" $cmdservme >> $patch/logs/smart.log
screen -S $srver -X eval "stuff '$cmdservme'\015"; fi
# остановка сервисов
if [ $stpsrv = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "свободной памяти:" $fmem", остановка сервисов" >> $patch/logs/smart.log
step=1
line=$(wc -l $patch/cfg/service-stop.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
service=$(head -n $step $patch/cfg/service-stop.cfg | tail -n 1)
service $service stop >> $patch/log/service-stop.log
step=$((step+1)); done; fi
# отключение модулей
if [ $mstmd = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "свободной памяти:" $fmem", отключение модулей" >> $patch/logs/smart.log
cp $patch/cfg/global.cfg $patch/cfg/global.cfg.old
timcheck=$(screen -ls | grep $ticms | wc -m | sed s/[^0-9]//g)
if [ $timcheck -gt 5 ]; then
sed 's/^timcom=.*/timcom=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
amecheck=$(screen -ls | grep $aumes | wc -m | sed s/[^0-9]//g)
if [ $amecheck -gt 5 ]; then
sed 's/^ames=.*/ames=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
intcheck=$(screen -ls | grep $inscr | wc -m | sed s/[^0-9]//g)
if [ $intcheck -gt 5 ]; then
sed 's/^intcom=.*/intcom=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
guacheck=$(screen -ls | grep $antichat | wc -m | sed s/[^0-9]//g)
if [ $guacheck -gt 5 ]; then
sed 's/^guardchat=.*/guardchat=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
anticheck=$(screen -ls | grep $anthascr | wc -m | sed s/[^0-9]//g)
if [ $anticheck -gt 5 ]; then
sed 's/^antihack=.*/antihack=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi
tmpback=$(screen -ls | grep $tbascnz | wc -m | sed s/[^0-9]//g)
if [ $tmpback -gt 5 ]; then
sed 's/^tempba=.*/tempba=0/g' $patch/cfg/global.cfg > $patch/sh/global2.cfg
mv $patch/sh/global2.cfg $patch/cfg/global.cfg; fi; fi; fi; fi

# проверка работы сервера
if [ $rebfix = "1" ]; then
dateh=`/bin/date '+%H'`
resth=$(echo $rest | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev)
jav=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if [ $dateh != $resth ] && [ $jav -lt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "сервер не работает, пуск" >> $patch/logs/smart.log
screen -dmUS $srver bash -c "$patch/start.sh"; fi; fi

# проверка антистопера
if [ $antifix = "1" ] && [ $antistop = "1" ]; then
check=$(ps ux | grep "antistoper.sh" | sed /grep/d | wc -m | sed s/[^0-9]//g)
if [ $check -lt 4 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "анстистопер не работает, запуск" >> $patch/logs/smart.log
antiche=$(screen -ls | grep $ansto | wc -m | sed s/[^0-9]//g)
if [ $antiche -gt 5 ]; then screen -S $ansto -X stuff "^C"; fi
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"; fi; fi

# проверка сервисов
if [ $chservs = "1" ]; then
# самое важное
zhcheck=$(screen -ls | grep $tsto | wc -m | sed s/[^0-9]//g)
if [ $zhki = "1" ] && [ $zhcheck -lt 5 ]; then
echo "запуск timestop.sh"
screen -dmUS $tsto bash -c "sleep 120 && $patch/sh/timestop.sh"; fi
ascheck=$(screen -ls | grep $autsav | wc -m | sed s/[^0-9]//g)
if [ $asw = "1" ] && [ $ascheck -lt 5 ]; then
echo "запуск autosave.sh"
screen -dmUS $autsav bash -c "sleep 120 && $patch/sh/autosave.sh"; fi
# проверка, отключались ли модули из-за перегрузки
if [ $monmem = "1" ]; then if [ $fmem -lt $minmem ]; then rmodul=0; fi; fi
if [ $moncpu = "1" ]; then if [ $chcpu -gt $mxsrmcp ]; then rmodul=0; fi; fi
if [ $rmodul = "0" ]; then
prohd=$((prohd+1))
if [ $prohd -gt 2 ]; then rmodul=1 && prohd=0; fi; fi
# запуск модулей, если они не стопались, или стопались 3 цикла назад
if [ $rmodul = "1" ]; then
timcheck=$(screen -ls | grep $ticms | wc -m | sed s/[^0-9]//g)
if [ $ticms = "1" ] && [ $timcheck -lt 5 ]; then
echo "запуск timecommand.sh"
screen -dmUS $timcomsc bash -c "sleep 120 && $patch/sh/timecommand.sh"; fi
amecheck=$(screen -ls | grep $aumes | wc -m | sed s/[^0-9]//g)
if [ $ames = "1" ] && [ $amecheck -lt 5 ]; then
echo "запуск automessage.sh"
screen -dmUS $aumes bash -c "sleep 120 && $patch/sh/automessage.sh"; fi
intcheck=$(screen -ls | grep $inscr | wc -m | sed s/[^0-9]//g)
if [ $intcom = "1" ] && [ $intcheck -lt 5 ]; then
echo "запуск intcom.sh"
screen -dmUS $inscr bash -c "sleep 120 && $patch/sh/intcom.sh"; fi
guacheck=$(screen -ls | grep $antichat | wc -m | sed s/[^0-9]//g)
if [ $guardchat = "1" ] && [ $guacheck -lt 5 ] && [ $log = "1" ]; then
echo "запуск chat.sh"
screen -dmUS $antichat bash -c "sleep 120 && $patch/sh/chat.sh"; fi
anticheck=$(screen -ls | grep $anthascr | wc -m | sed s/[^0-9]//g)
if [ $antihack = "1" ] && [ $anticheck -lt 5 ] && [ $log = "1" ]; then
echo "запуск antihack.sh"
screen -dmUS $anthascr bash -c "sleep 120 && $patch/sh/antihack.sh"; fi
tmpback=$(screen -ls | grep $tbascnz | wc -m | sed s/[^0-9]//g)
if [ $tempba = "1" ] && [ $tmpback -lt 5 ]; then
echo "запуск tempback.sh"
screen -dmUS $tbascnz bash -c "sleep 120 && $patch/sh/tempback.sh"; fi; fi; fi

smart=$(grep smart= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smpau=$(grep smpau= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
szchti=$(grep szchti= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
difilch=$(grep difilch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smonest=$(grep smonest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rebfix=$(grep rebfix= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
moncpu=$(grep moncpu= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
monmem=$(grep monmem= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
antifix=$(grep antifix= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chservs=$(grep chservs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $smonest = "1" ]; then exit 0; fi; done
#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

cpla=$(grep cpla= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
plday=$(grep plday= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
map=$(grep map= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
essentials=$(grep essentials= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
commandbook=$(grep commandbook= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
mcjobs=$(grep mcjobs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
herochat=$(grep herochat= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clpint=$(grep clpint= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clprid=$(grep clprid= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
Pl3xCommands=$(grep Pl3xCommands= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
Pl3xPerms=$(grep Pl3xPerms= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cleplas=$(grep cleplas= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $cleplas | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/clearplayer.log
screen -S $cleplas -X stuff "^C" >> $patch/logs/clearplayer.log; fi

if [ $clpint = "1" ]; then
if ! [ -f $patch/sh/cleplay.cfg ]; then echo "0" > $patch/sh/cleplay.cfg; fi
interval=`head -n $step $patch/sh/cleplay.cfg | tail -n 1`
if ! [ $interval = $clprid ]; then echo $((interval+1)) > $patch/sh/cleplay.cfg && exit 0; else echo "0" > $patch/sh/cleplay.cfg; fi; fi

if [ $cpla = "1" ]; then
folder="/$map/playerdata"
if [ -d $patch/$folder ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- запуск чистки" >> $patch/logs/clearplayer.log
step=1
cd $patch/$folder/
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="/$map/stats"
if [ -d $patch/$folder ]; then
step=1
cd $patch/$folder/
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins/Essentials/userdata"
if [ $essentials = "1" ] && [ -d $patch/$folder ]; then
step=1
cd $patch/$folder
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins/CommandBook/sessions"
if [ $commandbook = "1" ] && [ -d $patch/$folder ]; then
step=1
cd $patch/$folder
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins/mcjobs/data"
if [ $mcjobs = "1" ] && [ -d $patch/$folder ]; then
step=1
cd $patch/$folder
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins_mod/Pl3xCommands/userdata"
if [ $Pl3xCommands = "1" ] && [ -d $patch/$folder ]; then
step=1
cd $patch/$folder
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins_mod/Pl3xPerms/users"
if [ $Pl3xPerms = "1" ] && [ -d $patch/$folder ]; then
step=1
cd $patch/$folder
ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$file >> $patch/logs/clearplayer.log; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done; fi

folder="plugins/Herochat/chatters"
if [ $herochat = "1" ] && [ -d $patch/$folder/ ]; then
cd $patch/$folder/
fold=$(find . -type d | wc -l | sed s/[^0-9]//g)
find . -type d | sed 's/^[./]*//' > $patch/temp/folders.cfg
step2=1
while [ $((fold+2)) != $step2 ]; do
tfolder=$(head -n $step2 $patch/temp/folders.cfg | tail -n 1)
cd $patch/$folder/$tfolder/

ls > $patch/temp/players.txt
line=$(wc -l $patch/temp/players.txt | sed s/[^0-9]//g)
step=1
while [ $step != $((line+2)) ]; do
file=`head -n $step $patch/temp/players.txt | tail -n 1`
if [ -f $file ]; then
dchange=`stat -c '%y' $patch/$folder/$tfolder/$file | cut -c -10`
y=`echo $dchange | cut -c -4`
m=`echo $dchange | cut -c 6- | cut -c -2`
mch=`echo $m | cut -c -1` && if [ $mch = "0" ]; then m=`echo $m | cut -c 2-`; fi
d=`echo $dchange | cut -c 9-`
dch=`echo $d | cut -c -1` && if [ $dch = "0" ]; then d=`echo $d | cut -c 2-`; fi
dy=`/bin/date '+%Y'`
dm=`/bin/date '+%m'`
dmch=`echo $dm | cut -c -1` && if [ $dmch = "0" ]; then dm=`echo $dm | cut -c 2-`; fi
dday=`/bin/date '+%d'`
ddmch=`echo $dday | cut -c -1` && if [ $ddmch = "0" ]; then dday=`echo $dday | cut -c 2-`; fi

day=0
if [ $y != $dy ]; then dat=$((dy-y)) && dat=$((dat*360)) && day=`echo $dat`; fi
if [ $m != $dm ]; then dat=$((dm-m)) && dat=$((dat*30)) && day=$((day+dat)); fi
if [ $d != $dday ]; then dat=$((dday-d)) && day=$((day+dat)); fi
if [ $day -gt $plday ]; then
echo "удаление" $patch/$folder/$tfolder/$file "последнее изменение было дней назад:" $day >> $patch/logs/clearplayer.log
rm $patch/$folder/$tfolder/$file; fi; fi
step=$((step+1))
# экстренный прерыватель
if [ $step -gt $((line+3)) ]; then step=$(echo $((line+2))) && break; fi
percent=$((step*100/line))
if [ $ecper ]; then ecper=$((ecper+1)); else ecper=0; fi
if [ $ecper = "100" ]; then echo "проверено" $percent"% файлов в папке" $patch/$folder && ecper=0; fi; done

step2=$((step2+1)); done
rm $patch/temp/folders.cfg; fi; fi
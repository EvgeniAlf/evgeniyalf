#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

serstp=$(grep serstp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

if [ $serstp = "1" ]; then
step=1
line=$(wc -l $patch/cfg/service-stop.cfg | sed s/[^0-9]//g)
while [ $((line+2)) != $step ]; do
service=$(head -n $step $patch/cfg/service-stop.cfg | tail -n 1)
service $service stop >> $patch/log/service-stop.log
step=$((step+1)); done; fi
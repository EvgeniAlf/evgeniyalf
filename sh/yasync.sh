#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

ysnsc=$(grep ysnsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
cdzds=$(grep cdzds= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
lgin=$(grep lgin= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
passwd=$(grep passwd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yadisk=$(grep yadisk= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
davfs2=$(grep davfs2= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
webdav=$(grep webdav= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
kombode=$(grep kombode= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# проверки
ch1=$(echo $1 | wc -m | sed s/[^0-9]//g)
ch2=$(echo $2 | wc -m | sed s/[^0-9]//g)
ch3=$(echo $3 | wc -m | sed s/[^0-9]//g)
if [ "$ch1" -lt 5 ]; then exit 0; fi
if [ "$ch2" -gt 2 ] && [ "$2" = "none" ]; then ch2=0; fi
if [ "$ch3" -gt 2 ] && [ "$3" = "nodem" ]; then kombode=0; fi

# защита от двойного запуска
scrcheck=$(screen -ls | grep $ysnsc | wc -l | sed s/[^0-9]//g)
scrcheck2=$(screen -ls | grep $ysnsc"2" | wc -l | sed s/[^0-9]//g)
yacheck=$(ps ux | grep "yasync.sh" | sed /grep/d | sed /$$/d | wc -m | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ] || [ $scrcheck2 -gt 0 ] || [ $yacheck -gt 3 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date " - двойной запуск синхронизации" >> $patch/logs/yasync.log
if [ $yacheck -gt 3 ]; then
pid=$(pidof -x "yasync.sh" | sed /$$/d | head -n 1)
kill -s KILL $pid >> $patch/logs/yasync.log; fi
umount -t davfs $webdav >> $patch/logs/yasync.log
killall -s KILL "davfs2" >> $patch/logs/yasync.log
killall -s KILL "cadaver" >> $patch/logs/yasync.log
yandex-disk stop $patch/temp/syncdir/ >> $patch/logs/yasync.log
killall -s KILL "yandex-disk" >> $patch/logs/yasync.log
if [ $scrcheck -gt 1 ]; then screen -S $ysnsc -X stuff "^C" >> $patch/logs/yasync.log; fi
if [ $scrcheck2 -gt 0 ]; then screen -S $ysnsc"2" -X stuff "^C" >> $patch/logs/yasync.log; fi
if [ -f $patch/temp/yasync.log ]; then cat $patch/temp/yasync.log >> $patch/logs/yasync.log; fi
if [ -f $patch/temp/yabackfiles.cfg ]; then rm $patch/temp/yabackfiles.cfg >> $patch/logs/yasync.log; fi
if [ -f $patch/temp/yasync.log ]; then rm $patch/temp/yasync.log >> $patch/logs/yasync.log; fi; fi

# отдельный скрин для приложений (этот скрипт уже в скрине с этим названием, поэтому меняем его)
ysnsc=$(echo $ysnsc"2")

if [ $kombode = "1" ] && [ $yadisk = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "начало синхронизации с яндекс диском:" $date >> $patch/logs/yasync.log

# удаление старого бэкапа одним из двух способов
if [ $davfs2 = "1" ] && [ $ch2 -gt 5 ]; then
if ! [ -d /mnt/yandex.disk ]; then mkdir /mnt/yandex.disk; fi
screen -dmUS $ysnsc bash -c "mount -t davfs $webdav --target /mnt/yandex.disk/ | tee $patch/temp/yasync.log"
sleep $cdzds
echo "удаление старого бэкапа по webdav"
echo "авторизация"
screen -S $ysnsc -X eval "stuff '$lgin'\015"
sleep $cdzds
screen -S $ysnsc -X eval "stuff '$passwd'\015"
cat $patch/temp/yasync.log >> $patch/logs/yasync.log
rm $patch/temp/yasync.log
sleep $cdzds
delfile=$(echo $2 | rev | cut -f1 -d/ | rev)
echo "удаление старого бэкапа"
rm -rf /mnt/yandex.disk/$delfile/ >> $patch/logs/yasync.log
umount -t davfs $webdav >> $patch/logs/yasync.log; fi

if [ $davfs2 = "0" ] && [ $ch2 -gt 5 ]; then
screen -dmUS $ysnsc bash -c "cadaver $webdav | tee $patch/temp/yasync.log"
sleep $cdzds
echo "удаление старого бэкапа по webdav"
echo "авторизация"
while [ $auth = "on" ]; do
screen -S $ysnsc -X eval "stuff '$lgin'\015"
sleep $cdzds
aufail=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "Authentication required" | wc -m | sed s/[^0-9]//g)
if [ $aufail -lt 5 ]; then auth=off; fi; done
while [ $auth = "on" ]; do
sleep $cdzds
screen -S $ysnsc -X eval "stuff '$passwd'\015"
sleep $cdzds
aufail=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "Authentication required" | wc -m | sed s/[^0-9]//g)
if [ $aufail -lt 5 ]; then auth=off; fi; done
sleep $cdzds
delfile=$(echo $2 | rev | cut -f1 -d/ | rev)
echo "удаление старого бэкапа"
screen -S $ysnsc -X eval "stuff 'rmcol $delfile'\015"
sleep $cdzds
echo "выход"
screen -S $ysnsc -X eval "stuff 'quit'\015"
cat $patch/temp/yasync.log >> $patch/logs/yasync.log
rm $patch/temp/yasync.log; fi

# сама синхронизация
echo "начало синхронизации через демон яндекса"
finame=$(echo $1 | rev | cut -f1 -d/ | rev)
if ! [ -d $patch/temp/syncdir/ ]; then mkdir $patch/temp/syncdir/ >> $patch/logs/yasync.log; else
rm -rf $patch/temp/syncdir/* >> $patch/logs/yasync.log; fi
mv $1/ $patch/temp/syncdir/$finame/ >> $patch/logs/yasync.log
cd $patch/temp/syncdir/ >> $patch/logs/yasync.log
yandex-disk sync -d $patch/temp/syncdir/ --overwrite -a ~/.config/yandex-disk/passwd >> $patch/logs/yasync.log
mv $patch/temp/syncdir/$finame/ $1/ >> $patch/logs/yasync.log
rm -rf $patch/temp/syncdir/ >> $patch/logs/yasync.log
echo "закачка на яндекс диск завершена"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "синхронизация завершена:" $date >> $patch/logs/yasync.log
exit 0; fi

if [ $yadisk = "1" ] && [ $davfs2 = "1" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "начало синхронизации с яндекс диском по webdav:" $date >> $patch/logs/yasync.log
if ! [ -d /mnt/yandex.disk ]; then mkdir /mnt/yandex.disk; fi
screen -dmUS $ysnsc bash -c "mount -t davfs $webdav /mnt/yandex.disk/ | tee $patch/temp/yasync.log"
sleep $cdzds
echo "авторизация"
screen -S $ysnsc -X eval "stuff '$lgin'\015"
sleep $cdzds
screen -S $ysnsc -X eval "stuff '$passwd'\015"
sleep $cdzds
cat $patch/temp/yasync.log >> $patch/logs/yasync.log
rm $patch/temp/yasync.log
if [ $ch2 -gt 5 ]; then
sleep $cdzds
delfile=$(echo $2 | rev | cut -f1 -d/ | rev)
echo "удаление старого бэкапа"
rm -rf /mnt/yandex.disk/$delfile/ >> $patch/logs/yasync.log; fi
finame=$(echo $1 | rev | cut -f1 -d/ | rev)
echo "загрузка" $finame
cp -r $1/ /mnt/yandex.disk/$finame/ >> $patch/logs/yasync.log
umount -t davfs $webdav >> $patch/logs/yasync.log
echo "закачка на яндекс диск завершена"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "синхронизация завершена:" $date >> $patch/logs/yasync.log
exit 0; fi

if [ $yadisk = "1" ] && [ $davfs2 = "0" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "начало синхронизации с яндекс диском по webdav:" $date >> $patch/logs/yasync.log
screen -dmUS $ysnsc bash -c "cadaver $webdav | tee $patch/temp/yasync.log"
echo "авторизация"
sleep $cdzds
while [ $auth = "on" ]; do
screen -S $ysnsc -X eval "stuff '$lgin'\015"
sleep $cdzds
aufail=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "Authentication required" | wc -m | sed s/[^0-9]//g)
if [ $aufail -lt 5 ]; then auth=off; fi; done
while [ $auth = "on" ]; do
sleep $cdzds
screen -S $ysnsc -X eval "stuff '$passwd'\015"
sleep $cdzds
aufail=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "Authentication required" | wc -m | sed s/[^0-9]//g)
if [ $aufail -lt 5 ]; then auth=off; fi; done
sleep $cdzds
if [ $ch2 -gt 5 ]; then
sleep $cdzds
delfile=$(echo $2 | rev | cut -f1 -d/ | rev)
echo "удаление старого бэкапа"
screen -S $ysnsc -X eval "stuff 'rmcol $delfile'\015"; fi
sleep $cdzds
finame=$(echo $1 | rev | cut -f1 -d/ | rev)
echo "создание папки" $finame
screen -S $ysnsc -X eval "stuff 'mkcol $finame'\015"
screen -S $ysnsc -X eval "stuff 'cd /$finame'\015"
cd $1/
files=$(ls -l | grep ^- | awk '{print $9}' > $patch/temp/yabackfiles.cfg)
line=$(wc -l $patch/temp/yabackfiles.cfg | sed s/[^0-9]//g)
step=1
while [ $((line+1)) != $step ]; do
sleep $cdzds
file=`head -n $step $patch/temp/yabackfiles.cfg | tail -n 1`
filen=$(echo $file)
file=$(echo $1"/"$file)
echo "загрузка" $file
screen -S $ysnsc -X eval "stuff 'put $file'\015"
sleep="on"
while [ $sleep = "on" ]; do
downch=$(wc -l $patch/temp/yasync.log | sed s/[^0-9]//g)
downch=$((downch-2))
downch=$(echo $downch"d")
quich=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "connection timed out")
fail=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "failed:")
downch=$(cat $patch/temp/yasync.log | sed 1,$downch | grep "succeeded" | grep $filen)
downch=$(echo $downch | wc -m | sed s/[^0-9]//g)
quich=$(echo $quich | wc -m | sed s/[^0-9]//g)
fail=$(echo $fail | wc -m | sed s/[^0-9]//g)
if [ $downch -gt 3 ]; then sleep="off"; fi
if [ $quich -gt 3 ] || [ $fail -gt 3 ]; then sleep="off"; fi
if [ $sleep = "on" ]; then sleep 3; fi; done
step=$((step+1))
if [ $quich -gt 3 ] || [ $fail -gt 3 ]; then step=$((step-1)); fi; done

sleep $cdzds
echo "выход"
screen -S $ysnsc -X eval "stuff 'quit'\015"
cat $patch/temp/yasync.log >> $patch/logs/yasync.log
rm $patch/temp/yabackfiles.cfg
rm $patch/temp/yasync.log
echo "закачка на яндекс диск завершена"
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "синхронизация завершена:" $date >> $patch/logs/yasync.log; fi
exit 0
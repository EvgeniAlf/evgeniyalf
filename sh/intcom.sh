#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
intcom=$(grep intcom= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
inscr=$(grep inscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $inscr | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск intcom" $date >> $patch/logs/other.log
screen -S $inscr -X stuff "^C" >> $patch/logs/other.log; fi

while [ $intcom = "1" ]; do
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then
rm -rf $patch/temp/intcom/
exit 25; fi

step=1
line=$(wc -l $patch/cfg/intcom.cfg | sed s/[^0-9]//g)
while [ $step != $((line+2)) ]; do
com=`head -n $step $patch/cfg/intcom.cfg | tail -n 1 | sed -r 's/^[^=]+//' | cut -c 2-`
int=$(head -n $step $patch/cfg/intcom.cfg | tail -n 1 | rev | sed -r 's/^[^=]+//' | cut -c 2- | rev)
com2=$(head -n $step $patch/cfg/intcom.cfg | tail -n 1 | sed -r 's/^[^=]+//' | cut -c 2- | sed ':BEGIN;s/^\([^ ]\{0,29\}\) /\1/;tBEGIN')
if ! [ -d $patch/temp/intcom/ ]; then mkdir $patch/temp/intcom/; fi
if [ -f $patch/temp/intcom/$com2.cfg ]; then
int2=`head -n 1 $patch/temp/intcom/$com2.cfg | tail -n 1`
int2=$((int2-1))
echo $int2 > $patch/temp/intcom/$com2.cfg; else
echo $int > $patch/temp/intcom/$com2.cfg
int2=$(echo $int); fi
if [ $int2 = "0" ]; then
screen -S $srver -X eval "stuff '$com'\015"
echo $int > $patch/temp/intcom/$com2.cfg; fi
# на всякий случай
if [ $int2 -lt 0 ]; then
screen -S $srver -X eval "stuff '$com'\015"
echo $int > $patch/temp/intcom/$com2.cfg; fi
step=$((step+1))
sleep 1; done; done
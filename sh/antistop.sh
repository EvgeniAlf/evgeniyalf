#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена" > /dev/null; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

# чтобы потом грохнуть процесс, воизбежание двойного запуска
echo $$ > $patch/temp/antistop.pid

# ожидание 10мин, ждем пока сервер сам стартанет из автозапуска
if ! [ "$1" = "n" ]; then sleep 600; fi

antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
screench=$(grep screench= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
porch=$(grep porch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
maxstp=$(grep maxstp= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
port=$(grep port= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
rest=$(grep rest= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
preb=$(grep preb= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
psrab=$(grep psrab= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ansto=$(grep ansto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
chrazd=$(grep chrazd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
anticrash=$(grep anticrash= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
log=$(grep log= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
srver=$(grep srver= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smart=$(grep smart= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smtscr=$(grep smtscr= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
emptylg=$(grep emptylg= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
zhorebs=$(grep zhorebs= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

if [ "$1" = "n" ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- antistop перезапущен и работает" >> $patch/logs/antistoper.log; else
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- antistop запущен и работает" >> $patch/logs/antistop.log; fi

pstep=0
psrst=0
logche=0
oldlog=0
psr=0
while [ $antistop = "1" ]; do

#костыль от двойного запуска
pidnum=$(ps ux | grep "antistop.sh" | sed /grep/d | sed /$$/d | wc -l | sed s/[^0-9]//g)
if [ $pidnum -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- двойной запуск, выход" >> $patch/logs/antistop.log
exit 0; fi

# еще один костыль от выпадения, перезапуск
psrst=$((psrst+1))
if [ $psrst = "50" ]; then
sh $patch/sh/antistop.sh n &
exit 0
kill -s KILL $$; fi

# костыль на случай смены переменной
if [ $screench = "0" ]; then scr=1; fi

min=`/bin/date '+%M'`

# проверка screen
if [ $screench = "1" ]; then
check=`ps ux | grep screen | sed /grep/d | wc -m | awk '{print $1-1}'`
cheser=$(screen -ls | grep $srver | wc -m | sed s/[^0-9]//g)
if [ $check -gt 5 ] || [ $cheser -gt 3 ]; then scr=1; else
if [ $hour != $rhour ] && [ $cheser -lt 3 ]; then
pstep=$((pstep+1))
scr=0
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, screench" >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid
if [ $zhorebs = "1" ]; then
killall -s KILL java >> $patch/logs/antistop.log
screen -wipe >> $patch/logs/antistop.log
sleep 5
screen -dmUS $srver bash -c "$patch/start.sh" >> $patch/logs/antistop.log; else reboot; fi
if [ $preb = "1" ] && [ $psrab = $pstep ]; then
echo $date "- сработал антистоп, screench, лимит срабатываний, перезагрузка" >> $patch/logs/antistop.log
reboot && exit 0; fi
# тормозим цикп на 2мин, чтобы не запустить сервер много раз
# и костыль от перезапуска антистопером
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; elif [ $hour = $rhour ] && [ $min -gt $maxstp ]; then
pstep=$((pstep+1))
scr=0
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, screench" >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid
if [ $zhorebs = "1" ]; then
killall -s KILL java >> $patch/logs/antistop.log
screen -wipe >> $patch/logs/antistop.log
sleep 5
screen -dmUS $srver bash -c "$patch/start.sh" >> $patch/logs/antistop.log; else reboot; fi
if [ $preb = "1" ] && [ $psrab = $pstep ]; then
echo $date "- сработал антистоп, screench, лимит срабатываний, перезагрузка" >> $patch/logs/antistop.log
reboot && exit 0; fi
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; fi; fi; fi

# пинг порта
if [ $porch = "1" ]; then
echo "0" > $patch/temp/acheck.cfg
netcat -w3 -z localhost $port && echo "1" > $patch/temp/acheck.cfg
check=$(head -n 1 $patch/temp/acheck.cfg | tail -n 1)
if [ $check = "0" ]; then psr=$((psr+1)); else psr=0; fi
if [ $check = "0" ] && [ $psr -gt 20 ]; then
if [ $hour != $rhour ] && [ $scr = "1" ]; then
pstep=$((pstep+1))
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, porch" >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid
if [ $zhorebs = "1" ]; then killall -s KILL java >> $patch/logs/antistop.log; else reboot; fi
if [ $preb = "1" ] && [ $psrab = $pstep ]; then
echo $date "- сработал антистоп, porch, лимит срабатываний, перезагрузка" >> $patch/logs/antistop.log
reboot && exit 0; fi
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; elif [ $hour = $rhour ] && [ $min -gt $maxstp ] && [ $scr = "1" ]; then
pstep=$((pstep+1))
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, porch" >> $patch/logs/antistop.log
rm $patch/temp/antistop.pid
if [ $zhorebs = "1" ]; then killall -s KILL java >> $patch/logs/antistop.log; else reboot; fi


if [ $preb = "1" ] && [ $psrab = $pstep ]; then
echo $date "- сработал антистоп, porch, лимит срабатываний, перезагрузка" >> $patch/logs/antistop.log
reboot && exit 0; fi
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; fi; fi; fi

# выявление крашей
if [ $anticrash = "1" ] && [ -f $patch/server.log ] && [ $scr = "1" ]; then
crash=$(grep "ERROR]:" $patch/server.log | grep "This crash report has been saved to:" | grep $patch | grep "/crash-reports/" | grep "server.txt" | sed /$charzd/d | wc -m | sed s/[^0-9]//g)
crash2=$(grep "A fatal error has been detected" $patch/server.log | grep $patch/"hs_err_pid" | sed /$charzd/d | wc -m | sed s/[^0-9]//g)
if [ $crash -gt 5 ] || [ $crash2 -gt 5 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, anticrash" >> $patch/logs/antistop.log
if [ $zhorebs = "1" ]; then killall -s KILL java >> $patch/logs/antistop.log; else reboot; fi
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; fi; fi

# выявление краша по пустому логу
if [ $log = "1" ] && [ $emptylg = "1" ] && [ -f $patch/server.log ] && [ $scr = "1" ]; then
newlog=$(wc -l $patch/server.log | sed s/[^0-9]//g)
if [ $oldlog = "0" ]; then oldlog=$newlog; else
if [ $newlog = $oldlog ]; then logche=$((logche+1)); else logche=0; fi
if [ $logche -gt 15 ]; then screen -S $srver -X eval "stuff 'list'\015"; fi
if [ $logche -gt 40 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- сработал антистоп, emptylog" >> $patch/logs/antistop.log
if [ $zhorebs = "1" ]; then killall -s KILL java >> $patch/logs/antistop.log; else reboot; fi
screen -S $ansto -X stuff "^C"
screen -dmUS $ansto bash -c "$patch/sh/antistoper.sh"
sleep 120; fi; fi
oldlog=$newlog; fi

if [ $smart = "1" ]; then
checksm=$(ps ux | grep "smart.sh" | sed /grep/d | wc -m | sed s/[^0-9]//g)
if [ $checksm -lt 4 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "--->" $date "smart не работает, запуск" >> $patch/logs/antistop.log
scrism=$(screen -ls | grep $smtscr | wc -m | sed s/[^0-9]//g)
if [ $scrism -gt 5 ]; then screen -S $ansto -X stuff "^C"; fi
screen -dmUS $smtscr bash -c "$patch/sh/smart.sh"; fi; fi

antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
screench=$(grep screench= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
porch=$(grep porch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
preb=$(grep preb= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
psrab=$(grep psrab= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
smart=$(grep smart= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
sleep 5; done
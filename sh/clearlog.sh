#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

clel=$(grep clel= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
lsiz=$(grep lsiz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
yadisk=$(grep yadisk= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clstl=$(grep clstl= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
log=$(grep log= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
compress=$(grep compress= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
clebalo=$(grep clebalo= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`

if [ $clstl = "1" ] && [ $log = "1" ] && [ "$1" = "clear" ]; then
find $patch/logs/* -type f -name "*.gz" -delete >> $patch/logs/clearlog.log; fi

if [ "$1" = "cleback" ] && [ $clebalo = "1" ]; then
echo $date "- удаление лога бэкапирования и синхронизации" >> $patch/logs/clearlog.log
if [ -f $patch/logs/backup.log ]; then rm $patch/logs/backup.log >> $patch/logs/clearlog.log; fi
if [ -f $patch/logs/yasync.log ]; then rm $patch/logs/yasync.log >> $patch/logs/clearlog.log; fi; fi

if [ $clel = "1" ]; then
echo $date "- проверка размера папки логов" >> $patch/logs/clearlog.log
cd $patch/logs/
size=`du -s -B M | sed s/[^0-9]//g`
echo "вывод du -s -B M | sed s/[^0-9]//g:" >> $patch/logs/clearlog.log
du -s -B M | sed s/[^0-9]//g >> $patch/logs/clearlog.log
if [ $lsiz -lt $size ]; then
echo "очистка и архивирование логов" >> $patch/logs/clearlog.log
zip -r -$compress $patch/temp/logs.zip $patch/logs/ >> $patch/logs/clearlog.log
rm -rf $patch/logs/* >> $patch/logs/clearlog.log
if ! [ -d $patch/backups/logs/ ]; then mkdir $patch/backups/logs/; fi
mkdir $patch/backups/logs/$date/ >> $patch/logs/clearlog.log
mv $patch/temp/logs.zip $patch/backups/logs/$date/logs.zip >> $patch/logs/clearlog.log
if [ $yadisk = "1" ]; then
echo 'синхронизация с yandex disk'
echo 'синхронизация с yandex disk' >> $patch/logs/clearlog.log
$patch/sh/yasync.sh $patch/backups/logs; fi; fi; fi
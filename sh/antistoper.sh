#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ansto=$(grep ansto= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $ansto | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/antistoper.log
screen -S $ansto -X stuff "^C" >> $patch/logs/antistoper.log; fi

echo $$ > $patch/temp/antistoper.pid

sleep 610

while [ $antistop = "1" ]; do
check=$(ps ux | grep "antistop.sh" | sed /grep/d | wc -m | sed s/[^0-9]//g)
if [ $check -lt 4 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo $date "- антистоп вывалился, запуск" >> $patch/logs/antistoper.log
aspid=$(head -n 1 $patch/temp/antistop.pid| tail -n 1)
kill -s KILL $aspid >> $patch/logs/antistop.log
sh $patch/sh/antistop.sh n &
disown
sleep 10; fi
sleep 10
antistop=$(grep antistop= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done
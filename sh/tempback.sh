#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 5- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

tempba=$(grep tempba= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
bmode=$(grep bmode= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
compress=$(grep compress= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbints=$(grep tbints= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tembnu=$(grep tembnu= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tebsyd=$(grep tebsyd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
ysnsc=$(grep ysnsc= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbascnz=$(grep tbascnz= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)

# защита от двойного запуска
scrcheck=$(screen -ls | grep $tbascnz | wc -l | sed s/[^0-9]//g)
if [ $scrcheck -gt 1 ]; then
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "двойной запуск" $date >> $patch/logs/backup-temp.log
screen -S $tbascnz -X stuff "^C" >> $patch/logs/backup-temp.log; fi

date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "======================= старт скрипта:" $date "=======================" >> $patch/logs/backup-temp.log

if ! [ -d $patch/backups-temp/ ]; then mkdir $patch/backups-temp/ >> $patch/logs/backup-temp.log; fi
while [ $tempba = "1" ]; do
# если джава не запущена, нет смысла выполнения скрипта
jchek=`ps ux | grep java | sed /grep/d | wc -c | awk '{print $1-1}'`
if ! [ $jchek -gt 1 ]; then exit 25; fi

if [ $tbints -lt 5 ]; then tbints=5; fi
# пауза
sleep $((tbints*60))
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "=========> начало бэкапирования:" $date >> $patch/logs/backup-temp.log
# удаление старого бэкапа
cd $patch/backups-temp/
num=$(ls -l | grep ^d |wc -l)
old=$(ls -t $patch/backups-temp/ | tail -n1)
while [ "$((num+1))" -gt $tembnu ]; do
echo "удаление старого бэкапа "$old
echo "------> удаление старого бэкапа "$old >> $patch/logs/backup-temp.log
rm -rf $patch/backups-temp/$old/ >> $patch/logs/backup-temp.log
num=$(ls -l | grep ^d |wc -l)
old=$(ls -t $patch/backups-temp/ | tail -n1); done
new=$(ls -t -1rF | grep / | tail -1 | tr -d [=/=])
newch=$(echo $new | wc -l | sed s/[^0-9]//g)
if [ "$newch" -lt 3 ]; then new=none; fi

echo "------> создание нового бэкапа:" $date >> $patch/logs/backup-temp.log
mkdir $patch/backups-temp/$date/ >> $patch/logs/backup-temp.log
if ! [ -d $patch/temp/tempback/ ]; then mkdir $patch/temp/tempback/ >> $patch/logs/backup-temp.log; else
rm -rf $patch/temp/tempback/* >> $patch/logs/backup-temp.log; fi
line=$(wc -l $patch/cfg/tempback.cfg | sed s/[^0-9]//g)
step=1
while [ $((line+2)) != $step ]; do
file=`head -n $step $patch/cfg/tempback.cfg | tail -n 1`
cp -r $patch/$file $patch/temp/tempback/ >> $patch/logs/backup-temp.log
step=$((step+1)); done
zip -r -$compress $patch/backups-temp/$date/temp.zip $patch/temp/tempback/ >> $patch/logs/backup-temp.log
rm -rf $patch/temp/tempback/* >> $patch/logs/backup-temp.log

scheck=$(screen -ls | grep $ysnsc | wc -m | sed s/[^0-9]//g)
if [ $tebsyd = "1" ] && [ $scheck -lt 5 ]; then
echo "синхронизация с яндекс диском" >> $patch/logs/backup-temp.log
$patch/sh/yasync.sh $patch/backups-temp/$date $new nodem; fi
date=`/bin/date '+%H:%M:%S-%d.%m.%Y'`
echo "======================= конец бэкапирования ->" $date "<- =======================" >> $patch/logs/backup-temp.log
tempba=$(grep tempba= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tbints=$(grep tbints= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
tebsyd=$(grep tebsyd= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-); done

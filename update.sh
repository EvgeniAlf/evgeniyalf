#!/bin/bash
patch=`echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev`

errorch=$(grep errorch= $patch/cfg/global.cfg | sed -r 's/^[^=]+//' | cut -c 2-)
if [ $errorch = "1" ]; then $patch/sh/check.sh $$; elif [ $errorch = "0" ]; then echo "Проверка конфига не включена"; else
echo "Не верное значение переменной errorch в конфиге"
exit 0; fi

upd=0
if [ "$1" = "i" ]; then
# создание всех нужных файлов для корректной работы скрипта
mkdir $patch/logs/
mkdir $patch/temp/
mkdir $patch/temp/plugins/
mkdir $patch/cfg/
echo "http://dev.bukkit.org/bukkit-plugins/worldedit/" > $patch/cfg/update.cfg
echo "http://dev.bukkit.org/bukkit-plugins/nocheatplus/" >> $patch/cfg/update.cfg
echo "http://dev.bukkit.org/bukkit-plugins/worldborder/" >> $patch/cfg/update.cfg
echo "README.html" > $patch/cfg/remove.cfg
echo "CHANGELOG.txt" >> $patch/cfg/remove.cfg
echo "LICENSE.txt" >> $patch/cfg/remove.cfg
echo "contrib/" >> $patch/cfg/remove.cfg
echo " " > $patch/sh/update-ch.cfg
echo $patch"/logs/"
echo $patch"/temp/"
echo "----/plugins/"
echo $patch"/sh/"
echo "----update.cfg - адреса страниц плагинов для обновления"
echo "----remode.cfg - список файлов, которые нужно удалить перед копированием в папку plugins"
exit 25; elif [ "$1" = "ch" ]; then
# проверка обновлений
# алгоритм вырван из скачивания обновлений
# только вместо следующего шага загрузки - проверка ссылок
# если плагин обновился, то адрес ссылки на загрузку меняется
# (именно из-за него скрипт такой здоровый, но благодаря ему можно проверить обновления)
line=$(wc -l $patch/cfg/update.cfg | sed s/[^0-9]//g)
line2=$(wc -l $patch/sh/update-ch.cfg | sed s/[^0-9]//g)
echo "Строк в update.cfg:" $line >> $patch/logs/update.log
echo "Строк в update-ch.cfg:" $line2 >> $patch/logs/update.log
if [ "$line" != "$((line2-1))" ]; then
# -1, потому что echo добавляет еще одну пустую строку в конец файла
# в update.cfg не должно быть таких строк, чтобы весь скрипт работал корректно
echo "Список версий отсутствует, создание..."
rm $patch/sh/update-ch.cfg >> $patch/cfg/update.cfg
step=1

while [ $((line+2)) != $step ]; do
plugins=$(head -n $step $patch/cfg/update.cfg | tail -n 1)
cd $patch/temp/
wget -a $patch/logs/update.log -c $plugins
link=`grep "Download</a>" index.html`
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
echo $link > index.html
link=`sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}'`
sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}' >> $patch/logs/update.log
check=$(grep http://dev.bukkit.org index.html)
check=`echo $check | wc -m`
if [ $check = "1" ]; then link=`echo "http://dev.bukkit.org"$link`; fi
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
echo $link >> $patch/sh/update-ch.cfg
echo "Добавлено:" $link
step=$((step+1)); done
echo "Список версий создан"
exit 25; fi

step=1
while [ $((line+2)) != $step ]; do
plugins=$(head -n $step $patch/cfg/update.cfg | tail -n 1)
cd $patch/temp/
wget -a $patch/logs/update.log -c $plugins
link=`grep "Download</a>" index.html`
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
echo $link > index.html
link=`sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}'`
sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}' >> $patch/logs/update.log
check=$(grep http://dev.bukkit.org index.html)
check=`echo $check | wc -m`
if [ $check = "1" ]; then link=`echo "http://dev.bukkit.org"$link`; fi
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
echo $link > index.html

com=$(head -n $step $patch/sh/update-ch.cfg | tail -n 1)
result=`grep $com index.html`
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
check=`echo $result | wc -m`
echo "Проверка:" $plugins
if [ $check = "1" ]; then
echo "Кажется, этот нужно обновить:" $plugins
echo "Кажется, этот нужно обновить:" $plugins >> $patch/logs/update.log
echo $plugins >> $patch/temp/list.cfg
upd=$((upd+1)); fi
step=$((step+1)); done
# конец проверки обновлений, вывод результатов и завершение скрипта
echo "Проверка обновлений завершена"
if [ $upd = "0" ]; then
echo "Обновления не найдены"
exit 25; else
echo "Эти плагины, кажется, нуждаются в обновлении:"
while read line; do
echo "Эти плагины, кажется, нуждаются в обновлении: $line" >> $patch/logs/update.log
echo "$line"; done < $patch/temp/list.cfg
cp $patch/temp/list.cfg $patch/UPDATE-PL.txt >> $patch/logs/update.log
rm $patch/temp/list.cfg >> $patch/logs/update.log; fi; fi

if [ "$1" = "auto" ]; then who=n; elif [ "$2" = "up" ]; then who=u; elif [ "$2" = "ch" ]; then exit 25; else
echo "Обновить 1 плагин или все?"
echo "y - один"
echo "n - все"
if [ $upd != "0" ]; then echo "u - только устаревшие"; fi
read who
if [ $who = 'y' ]; then
echo "Введите название строчными буквами"
read name; elif [ $who = 'n' ]; then echo "Начинаем обновление..."; elif [ $who = "u" ]; then echo "Начинаем обновление устаревших плагинов..."; else
# защита от дурака
echo "ответ на вопрос: "$who >> $patch/logs/update.log
echo "Неверный ввод (не y, не n и не u)"
exit 25; fi; fi

step=1
# если обновляем только старые - происходит подмена списка
# и -1, потому что echo оставляет пустую строку в конце
if [ $who = "u" ]; then
line=$(wc -l $patch/UPDATE-PL.txt | sed s/[^0-9]//g)
line=$((line-1)); else
line=$(wc -l $patch/cfg/update.cfg | sed s/[^0-9]//g); fi
date=$(/bin/date '+%H:%M:%S-%d.%m.%Y')
echo "Начало обновления:" $date >> $patch/logs/update.log
echo "Указанный patch:" $patch >> $patch/logs/update.log
echo "Количество строк в sh/update.cfg или UPDATE-PL.txt:" $line >> $patch/logs/update.log
if [ $who = 'y' ]; then
echo "название: "$name >> $patch/logs/update.log; fi

rm -rf $patch/temp/plugins/* >> $patch/logs/update.log
while [ $((line+2)) != $step ]; do
# +1, потому что step начинается с 1 а не с 0 (нет нулевой строки в файле)
# еще +1 чтобы обновил последний плагин (иначе условие != не выполняется на последней строке в файле)
if [ $who = 'y' ]; then
# если ответ да, то по названию находим строку и узнаем ее номер
down=$(grep -n $name $patch/cfg/update.cfg | sed s/[^0-9]//g)
grep -n $name $patch/cfg/update.cfg | sed s/[^0-9]//g >> $patch/logs/update.log
# и ставим step на нужную строчку, дальше алгоритм сделает все сам
step=$down; fi
# выдергиваем строчку со ссылкой
if [ $who = "u" ]; then
plugins=$(head -n $step $patch/UPDATE-PL.txt | tail -n 1)
head -n $step $patch/UPDATE-PL.txt | tail -n 1 >> $patch/logs/update.log; else
plugins=$(head -n $step $patch/cfg/update.cfg | tail -n 1)
head -n $step $patch/cfg/update.cfg | tail -n 1 >> $patch/logs/update.log; fi
cd $patch/temp/
echo "Приступаем("$step"):" $plugins 
wget -a $patch/logs/update.log -c $plugins
# выдергиваем строку с искомой фразой (на страницу загрузки)
link=`grep "Download</a>" index.html`
grep "Download</a>" index.html >> $patch/logs/update.log
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
echo $link > index.html
# обрабатываем строку, оставляя только чистую ссылку
link=`sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}'`
sed '/">D/ {s//\n/;P;Q}' index.html | rev | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}' >> $patch/logs/update.log
# начало проверки на наличие куска адреса (часто ссылки без него)
check=$(grep http://dev.bukkit.org index.html)
check=`echo $check | wc -m`
# и если адреса нет, то добавляем его к нашей переменной с очищенной ссылкой
if [ $check = "1" ]; then link=`echo "http://dev.bukkit.org"$link`; fi
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
# замена ссылки в файле сверки версий, чтобы плагин считался обновленным
numbpl=$(cat $patch/cfg/update.cfg | grep -n "$plugins" | rev | sed -r 's/^[^:]+//' | cut -c 2- | rev | sed s/[^0-9]//g)
echo $plugins
echo $numbpl
oldstp=$(head -n $numbpl $patch/sh/update-ch.cfg | tail -n 1)
sed "s%$oldstp%$link%g" $patch/sh/update-ch.cfg > $patch/sh/update-ch2.cfg
cp $patch/sh/update-ch.cfg $patch/sh/update-ch.cfg.old >> $patch/logs/update.log
mv $patch/sh/update-ch2.cfg $patch/sh/update-ch.cfg >> $patch/logs/update.log
wget -a $patch/logs/update.log -c $link
link=`grep "Download</a>" index.html`
grep "Download</a>" index.html >> $patch/logs/update.log
find $patch/temp/* -type f -name "*html*" -delete
echo $link > index.html
# та же самая процедура очистки и проверки
link=`rev index.html | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}'`
rev index.html | sed '/"=ferh/ {s//\n/;P;Q}' | rev | sed '/">D/ {s//\n/;P;Q}' >> $patch/logs/update.log
check=$(grep http://dev.bukkit.org index.html)
check=`echo $check | wc -m`
if [ $check = "1" ]; then link=`echo "http://dev.bukkit.org"$link`; fi
find $patch/temp/* -type f -name "*html*" -delete >> $patch/logs/update.log
cd $patch/temp/plugins/
echo "Загружаем:" $link
wget -a $patch/logs/update.log -c $link
echo "ОК! Едем дальше..."
if [ $who = 'y' ]; then
# а тут завершение цикла, если это нужно
# если грохнуть exit-ом, то нижняя часть скрипта не выполнится
# поэтому увеличиваем step чтобы while завершился
step=$((line+2)); else
step=$((step+1)); fi done
cd $patch/temp/

# проверка на архивы и разорхивирование их
find $patch/temp/plugins/ -iname '*.zip' > $patch/temp/archive.txt
find $patch/temp/plugins/ -iname '*.zip' >> $patch/logs/update.log
zips=$(wc -m $patch/temp/archive.txt | sed s/[^0-9]//g)
if [ $zips -gt 3 ]; then
# если в archive.txt текста больше 3 символов, значит там список архивов
archives=$(wc -l $patch/temp/archive.txt | sed s/[^0-9]//g)
step=0
while [ $archives != $step ]; do
# тут step с 0, поэтому +1 не нужно
unpack=$(head -n $((step+1)) $patch/temp/archive.txt | tail -n 1)
# а вот тут нужно, ведь нет нулевой строки в файле
# просто по-другому распределил этот +1 (зачем-то...)
head -n $((step+1)) $patch/temp/archive.txt | tail -n 1 >> $patch/logs/update.log
echo "Разорхивирование" $unpack
echo "Разорхивирование" $unpack >> $patch/logs/update.log
unzip -o $unpack -d $patch/temp/plugins/ >> $patch/logs/update.log
rm $unpack >> $patch/logs/update.log
step=$((step+1)); done; fi
rm $patch/temp/archive.txt >> $patch/logs/update.log

# если качались (следовательно, затем распаковывались) архивы и список на удаление не пуст, то приступаем
if [ $zips -gt 3 ]; then
del=$(wc -m $patch/cfg/remove.cfg | sed s/[^0-9]//g)
if [ $del -gt 3 ]; then
rem=$(wc -l $patch/cfg/remove.cfg | sed s/[^0-9]//g)
step=0
while [ $((rem+1)) != $step ]; do
# +1 чтобы удалился последий файл (на последней строке условие != не выполняется)
# как мы помним, я по-другому распределил +1 в коде
# нежели в алгоритме загрузки плагинов О_о
tdel=$(head -n $((step+1)) $patch/cfg/remove.cfg | tail -n 1)
head -n $((step+1)) $patch/cfg/remove.cfg | tail -n 1 >> $patch/logs/update.log
echo "Удаление" $tdel
echo "Удаление" $tdel >> $patch/logs/update.log
rm -rf $patch/temp/plugins/$tdel >> $patch/logs/update.log
step=$((step+1)); done; fi; fi

# конечный этап - копирование в папку плагинов
echo "Копирование плагинов..."
echo "Содержимое папки ~/temp/plugins/ перед копированием:" >> $patch/logs/update.log
ls $patch/temp/plugins/ >> $patch/logs/update.log
cp -R $patch/temp/plugins/* $patch/plugins/ >> $patch/logs/update.log
echo "Копирование завершено! Очистка временной папки..."
rm -r $patch/temp/plugins/* >> $patch/logs/update.log
echo "Содержимое папки ~/plugins/ после копирования:" >> $patch/logs/update.log
ls $patch/plugins/ >> $patch/logs/update.log

# перееминовываем файл, чтобы не приходило ложное уведомление в игру
if [ $who = "u" ]; then mv $patch/UPDATE-PL.txt $patch/LAST-UPDATE.txt >> $patch/logs/update.log; fi

echo "Обновление завершено!"

date2=$(/bin/date '+%H:%M:%S-%d.%m.%Y')
echo "Обновление завершено ("$date" - "$date2")" >> $patch/logs/update.log
# ls на всякий случай, вдруг копирнулось что-то лишнее
# ведь названия плагинов могли изменится или еще чего-нибудь добавили в архивы
ls $patch/plugins/
# логи - полезная штука, поэтому тут полно записей в лог >> $patch/logs/update.log
# без них трудно ловить баги, а они могут возникнуть
# в случае изменения сайта dev.bukkit.org или криворукости юзера xD